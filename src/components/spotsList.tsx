import React from "react";

import { LinearProgress, makeStyles, Paper, Table, TableBody, TableContainer, TableHead, TableRow } from "@material-ui/core";

import { Spot } from "@redux/spots/models";

import { StyledTableCell, StyledTableRow } from "@styles/table";

type ComponentProps = {
    loading: boolean;
    error?: string;
    spots: Spot[];
    style?: object;
};

const useStyles = makeStyles({
    root: {
        width: "100%",
        display: "block",
    },
    table: {
        maxHeight: "100%"
    }
});

const SpotsList = (props: ComponentProps) => {
    const { loading, error, spots, style } = props;
    const classes = useStyles();

    return (
        <TableContainer style={style} className={classes.root} component={Paper}>
            {loading && <LinearProgress />}
            <Table stickyHeader className={classes.table} aria-label="customized table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell align="left">Name</StyledTableCell>
                        <StyledTableCell align="left">Country</StyledTableCell>
                        <StyledTableCell align="left">Latitude</StyledTableCell>
                        <StyledTableCell align="left">Longitude</StyledTableCell>
                        <StyledTableCell align="left">Wind Prob.</StyledTableCell>
                        <StyledTableCell align="left">When to go</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {spots.map((spot) => (
                        <StyledTableRow key={spot.id}>
                            <StyledTableCell align="left">{spot.name}</StyledTableCell>
                            <StyledTableCell align="left">{spot.country}</StyledTableCell>
                            <StyledTableCell align="left">{spot.lat}</StyledTableCell>
                            <StyledTableCell align="left">{spot.long}</StyledTableCell>
                            <StyledTableCell align="left">{spot.probability}</StyledTableCell>
                            <StyledTableCell align="left">{spot.month}</StyledTableCell>
                        </StyledTableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default SpotsList;
