import React, { ChangeEvent, FormEvent, useEffect, useRef, useState } from "react";

import { Button, CircularProgress, Grid, Icon, LinearProgress, Paper, TextField, Theme, Typography } from "@material-ui/core";
import { createStyles, makeStyles } from "@material-ui/styles";

import { Login } from "@redux/auth/models";

type ComponentProps = {
    authenticate: (loginData: Login) => void;
    error?: string;
    loading: boolean;
};

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        loginForm: {
            justifyContent: "center",
            minHeight: "90vh"
        },
        buttonBlock: {
            marginTop: "1rem",
            position: "relative"
        },
        loginBackground: {
            justifyContent: "center",
            minHeight: "30vh",
            padding: "3.5rem"
        },
        forgotPassword: {
            color: theme.palette.primary.light,
            fontSize: theme.typography.fontSize,

            "&:hover": {
                backgroundColor: "transparent",
                color: theme.palette.primary.main
            }
        },
        buttonProgress: {
            color: theme.palette.primary.main,
            position: "absolute",
            top: "50%",
            left: "50%",
            marginTop: -12,
            marginLeft: -12
        }
    })
);

const LoginForm = (props: ComponentProps) => {
    const { error, loading, authenticate } = props;
    const classes = useStyles();
    const [formData, setFormData] = useState({ email: "", password: "" });
    const [formValid, setFormValid] = useState(false);
    const form = useRef<HTMLFormElement>(null);

    useEffect(() => {
        setFormValid(form.current?.checkValidity() || false);
    }, []);

    const changeFormData = (e: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        switch (name) {
            case "email":
                setFormData((state) => ({
                    ...state,
                    email: value
                }));
                break;
            case "password":
                setFormData((state) => ({
                    ...state,
                    password: value
                }));
                break;
            default:
                break;
        }
        setFormValid(form.current?.checkValidity() || false);
    };

    const submit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        authenticate(formData);
    };

    return (
        <Grid container direction="column" justify="center" className={classes.loginForm}>
            {loading && <LinearProgress />}
            <Paper variant="elevation" elevation={2} className={classes.loginBackground}>
                <Grid item>
                    <Typography variant="h6">Sign in</Typography>
                </Grid>
                <Grid item>
                    <form ref={form} onSubmit={submit}>
                        {error && (
                            <Typography variant="subtitle2" color="error">
                                Error: {error}
                            </Typography>
                        )}
                        <Grid container direction="column">
                            <Grid item>
                                <TextField
                                    type="email"
                                    label="Email"
                                    value={formData.email}
                                    onChange={changeFormData}
                                    fullWidth
                                    name="email"
                                    required
                                    autoFocus
                                />
                            </Grid>
                            <Grid item>
                                <TextField
                                    type="password"
                                    label="Password"
                                    value={formData.password}
                                    onChange={changeFormData}
                                    fullWidth
                                    name="password"
                                    required
                                />
                            </Grid>
                            <Grid item className={classes.buttonBlock}>
                                <Button
                                    startIcon={<Icon>login</Icon>}
                                    disabled={loading || !formValid}
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    type="submit"
                                >
                                    Submit
                                </Button>
                                {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
                <Grid item>
                    <Button className={classes.forgotPassword}>
                        Forgot Password?
                    </Button>
                </Grid>
            </Paper>
        </Grid>
    );
};

export default LoginForm;
