import React from "react";

import moment from "moment";

import { Avatar, LinearProgress, Link, makeStyles, Paper, Table, TableBody, TableContainer, TableHead, TableRow } from "@material-ui/core";

import { User } from "@redux/users/models";

import { StyledTableCell, StyledTableRow } from "@styles/table";

type ComponentProps = {
    loading: boolean;
    error?: string;
    users: User[];
};

const useStyles = makeStyles({
    table: {
        minWidth: 700
    }
});

const UsersList = (props: ComponentProps) => {
    const { loading, error, users } = props;
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            {loading && <LinearProgress />}
            <Table className={classes.table} aria-label="customized table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell></StyledTableCell>
                        <StyledTableCell align="left">Name</StyledTableCell>
                        <StyledTableCell align="left">Email</StyledTableCell>
                        <StyledTableCell align="left">Created at</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {users.map((user) => (
                        <StyledTableRow key={user.id}>
                            <StyledTableCell component="th" scope="row">
                                <Avatar alt={user.name} src={user.avatar} />
                            </StyledTableCell>
                            <StyledTableCell align="left">{user.name}</StyledTableCell>
                            <StyledTableCell align="left">
                                <Link href={`email:${user.email}`}>{user.email}</Link>
                            </StyledTableCell>
                            <StyledTableCell align="left">{moment(user.createdAt).format("DD MMM YYYY")}</StyledTableCell>
                        </StyledTableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default UsersList;
