import React from "react";
import { ComposableMap, Geographies, Geography, Marker, ZoomableGroup } from "react-simple-maps";

import { createStyles, LinearProgress, makeStyles, Theme } from "@material-ui/core";

import { Spot } from "@redux/spots/models";

type ComponentProps = {
    loading: boolean;
    error?: string;
    favoriteSpots: Spot[];
    maxHeight: string;
    selectMarker: Function;
};

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        geography: {
            fill: theme.palette.grey[300],
            outline: "none",

            "&:hover": {
                fill: theme.palette.info.light
            }
        },
        marker: {
            transform: "translate(-12px, -24px)",
            fill: "none",
            stroke: theme.palette.info.main,
            strokeWidth: "2",
            strokeLinecap: "round",
            strokeLinejoin: "round"
        },
        markerWrapper: {
            cursor: "pointer"
        }
    })
);

const FavoritesMapList = (props: ComponentProps) => {
    const { loading, error, favoriteSpots, maxHeight, selectMarker } = props;
    const classes = useStyles();
    const geoUrl = "/world-map.json";

    return (
        <>
            {loading && <LinearProgress />}
            <ComposableMap style={{ maxHeight }}>
                <ZoomableGroup>
                    <Geographies geography={geoUrl}>
                        {({ geographies }) =>
                            geographies.map((geo) => <Geography className={classes.geography} key={geo.rsmKey} geography={geo} />)
                        }
                    </Geographies>
                    {favoriteSpots.map((spot) => (
                        <Marker
                            onClick={() => {
                                selectMarker(spot);
                            }}
                            className={classes.markerWrapper}
                            key={spot.name}
                            coordinates={[Number(spot.lat), Number(spot.long)]}
                        >
                            <g className={classes.marker}>
                                <circle cx="12" cy="10" r="3" />
                                <path d="M12 21.7C17.3 17 20 13 20 10a8 8 0 1 0-16 0c0 3 2.7 6.9 8 11.7z" />
                            </g>
                            <text textAnchor="middle" y="15">
                                {spot.name}
                            </text>
                        </Marker>
                    ))}
                </ZoomableGroup>
            </ComposableMap>
        </>
    );
};

export default FavoritesMapList;
