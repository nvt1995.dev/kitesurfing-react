import React, { Fragment, useState } from "react";
import { Link, NavLink, useLocation } from "react-router-dom";

import {
    AppBar,
    Avatar,
    Breadcrumbs,
    Button,
    Grid,
    Icon,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Menu,
    MenuItem,
    SwipeableDrawer,
    Theme,
    Toolbar,
    Typography
} from "@material-ui/core";
import { createStyles, makeStyles } from "@material-ui/styles";

import { MenuItem as Menu_Item } from "@models/general";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        menuButton: {
            marginRight: theme.spacing(2)
        },
        link: {
            textDecoration: "none",
            color: theme.palette.primary.light,

            "&.active": {
                color: theme.palette.primary.main
            }
        },
        linkIcon: {
            color: "inherit"
        },
        breadcrumbs: {
            padding: `${theme.spacing(1, 3)}`
        },
        breadcrumbLink: {
            textDecoration: "none",
            color: theme.palette.primary.light
        },
        userMenuBtn: {
            marginLeft: "auto"
        }
    })
);

type ComponentStoreProps = {
    isAuthenticated: boolean;
    menuItems: Menu_Item[];
    logout: Function;
};

const Header = (props: ComponentStoreProps) => {
    const { isAuthenticated, menuItems, logout } = props;
    const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
    const [drawer, setDrawer] = useState(false);
    const classes = useStyles();
    const location = useLocation();
    const openUserMenu = Boolean(anchorEl);

    const handleUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleCloseUserMenu = () => {
        setAnchorEl(null);
    };

    const logOut = () => {
        handleCloseUserMenu();
        logout();
    };

    const toggleDrawer = (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
        if (
            event &&
            event.type === "keydown" &&
            ((event as React.KeyboardEvent).key === "Tab" || (event as React.KeyboardEvent).key === "Shift")
        ) {
            return;
        }

        setDrawer(open);
    };

    const getMenuLabel = (path: string) => {
        const menuItem = menuItems.find((m) => m.path === `/${path}`);
        return menuItem?.label || path;
    };

    return (
        <Grid container>
            <AppBar position="static">
                <Toolbar>
                    {isAuthenticated && (
                        <IconButton
                            onClick={toggleDrawer(true)}
                            edge="start"
                            className={classes.menuButton}
                            color="inherit"
                            aria-label="menu"
                        >
                            <Icon>menu</Icon>
                        </IconButton>
                    )}
                    {isAuthenticated && (
                        <Fragment>
                            <IconButton
                                className={classes.userMenuBtn}
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleUserMenu}
                                color="inherit"
                            >
                                <Avatar src="/no-user.png" />
                            </IconButton>
                            <Menu
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: "top",
                                    horizontal: "right"
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: "top",
                                    horizontal: "right"
                                }}
                                open={openUserMenu}
                                onClose={handleCloseUserMenu}
                            >
                                <MenuItem component={Link} to="/profile" onClick={handleCloseUserMenu}>
                                    <Button startIcon={<Icon>account_circle</Icon>}>Profile</Button>
                                </MenuItem>
                                <MenuItem component={Link} to="/my-account" onClick={handleCloseUserMenu}>
                                    <Button startIcon={<Icon>settings</Icon>}>Account</Button>
                                </MenuItem>
                                <MenuItem onClick={logOut}>
                                    <Button startIcon={<Icon>exit_to_app</Icon>}>Logout</Button>
                                </MenuItem>
                            </Menu>
                            <SwipeableDrawer anchor="left" open={drawer} onClose={toggleDrawer(false)} onOpen={toggleDrawer(true)}>
                                <div role="presentation" onClick={toggleDrawer(false)} onKeyDown={toggleDrawer(false)}>
                                    <List>
                                        {menuItems.map((item, index) =>
                                            item.showInMenu ? (
                                                <NavLink exact className={classes.link} activeClassName="active" key={index} to={item.path}>
                                                    <ListItem>
                                                        <ListItemIcon className={classes.linkIcon}>
                                                            <Icon>{item.icon}</Icon>
                                                        </ListItemIcon>
                                                        <ListItemText primary={item.label} />
                                                    </ListItem>
                                                </NavLink>
                                            ) : (
                                                ""
                                            )
                                        )}
                                    </List>
                                </div>
                            </SwipeableDrawer>
                        </Fragment>
                    )}
                </Toolbar>
            </AppBar>
            {isAuthenticated && (
                <Breadcrumbs className={classes.breadcrumbs} aria-label="breadcrumb">
                    {location.pathname.split("/").map((path, index, paths) => {
                        if (index === paths.length - 1) {
                            return (
                                <Typography key={index} color="primary">
                                    {getMenuLabel(path)}
                                </Typography>
                            );
                        }

                        return (
                            <Link key={index} className={classes.breadcrumbLink} to={`/${path}`}>
                                {index === 0 ? "Kitesurfing" : getMenuLabel(path)}
                            </Link>
                        );
                    })}
                </Breadcrumbs>
            )}
        </Grid>
    );
};

export default Header;
