import React, { ChangeEvent, FormEvent, useEffect, useRef, useState } from "react";

import {
    Backdrop,
    Button,
    createStyles,
    Grid,
    Icon,
    LinearProgress,
    makeStyles,
    Paper,
    TextField,
    Theme,
    Typography
} from "@material-ui/core";

import { Spot } from "@redux/spots/models";

type ComponentProps = {
    loading: boolean;
    error?: string;
    setSelectedSpot: Function;
    spot: Spot;
    saveSpot: Function;
};

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            zIndex: theme.zIndex.modal
        },
        grid: {
            justifyContent: "center",
            height: "100vh"
        },
        buttonBlock: {
            marginTop: "1rem",
            position: "relative",
            display: "flex",
            justifyContent: "flex-end",

            "& >:first-child": {
                marginRight: theme.spacing(2)
            }
        },
        background: {
            justifyContent: "center",
            minHeight: "30vh",
            padding: "3.5rem"
        }
    })
);

const SpotContent = (props: ComponentProps) => {
    const { loading, error, setSelectedSpot, spot, saveSpot } = props;
    const classes = useStyles();
    const [cloneSpot, setCloneSpot] = useState(spot);
    const [formValid, setFormValid] = useState(false);
    const form = useRef<HTMLFormElement>(null);

    useEffect(() => {
        setFormValid(form.current?.checkValidity() || false);
    }, []);

    const submit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (spot.name === cloneSpot.name) {
            return;
        }
        saveSpot(cloneSpot);
        setSelectedSpot(null);
    };

    const changeFormData = (e: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        switch (name) {
            case "name":
                setCloneSpot((state) => ({
                    ...state,
                    name: value
                }));
                break;
            default:
                break;
        }
        setFormValid(form.current?.checkValidity() || false);
    };

    const onBackgroundClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        if ((event.target as HTMLElement).classList.contains("MuiGrid-root")) {
            setSelectedSpot(null);
        }
    };

    return (
        <Backdrop open={true} onClick={onBackgroundClick} className={classes.root}>
            <Grid container justify="center" direction="row">
                <Grid item>
                    <Grid container direction="column" justify="center" className={classes.grid}>
                        {loading && <LinearProgress />}
                        <Paper variant="elevation" elevation={2} className={classes.background}>
                            <Grid item>
                                <form ref={form} onSubmit={submit}>
                                    {error && (
                                        <Typography variant="body2" color="error">
                                            Error: {error}
                                        </Typography>
                                    )}
                                    <Grid container direction="column">
                                        <Grid item>
                                            <TextField
                                                type="text"
                                                value={cloneSpot.name}
                                                onChange={changeFormData}
                                                label="Name"
                                                fullWidth
                                                name="name"
                                                required
                                                autoFocus
                                            />
                                        </Grid>
                                        <Grid item>
                                            <TextField
                                                InputProps={{
                                                    readOnly: true
                                                }}
                                                type="text"
                                                value={cloneSpot.country}
                                                label="Country"
                                                fullWidth
                                                name="country"
                                            />
                                        </Grid>
                                        <Grid item>
                                            <TextField
                                                InputProps={{
                                                    readOnly: true
                                                }}
                                                type="text"
                                                value={spot.month}
                                                label="High Season"
                                                fullWidth
                                                name="month"
                                            />
                                        </Grid>
                                        <Grid item>
                                            <TextField
                                                InputProps={{
                                                    readOnly: true
                                                }}
                                                type="text"
                                                value={spot.probability}
                                                label="Wind Probability"
                                                fullWidth
                                                name="probability"
                                            />
                                        </Grid>
                                        <Grid item className={classes.buttonBlock}>
                                            <Button
                                                onClick={() => {
                                                    setSelectedSpot(null);
                                                }}
                                                startIcon={<Icon>cancel</Icon>}
                                                disabled={loading}
                                                variant="contained"
                                                color="secondary"
                                                type="button"
                                            >
                                                Cancel
                                            </Button>
                                            <Button
                                                startIcon={<Icon>save</Icon>}
                                                disabled={loading || !formValid}
                                                variant="contained"
                                                color="primary"
                                                type="submit"
                                            >
                                                Save
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </form>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </Backdrop>
    );
};

export default SpotContent;
