import React, { Dispatch, useEffect } from "react";
import { connect } from "react-redux";

import { InitialState } from "@redux/store-models";
import { getList } from "@redux/users/actions";
import { selectError, selectList, selectLoading } from "@redux/users/selectors";

import UsersList from "@components/usersList";

const UsersPage = (props: ComponentStoreProps & ComponentDispatchProps) => {
    const { loading, error, getUserList, users } = props;

    useEffect(getUserList, [getUserList]);

    return <UsersList users={users} loading={loading} error={error} />;
};

type ComponentStoreProps = ReturnType<typeof mapStateToProps>;
type ComponentDispatchProps = ReturnType<typeof mapDispatchToProps>;

const mapStateToProps = (state: InitialState) => ({
    loading: selectLoading(state.users),
    error: selectError(state.users),
    users: selectList(state.users)
});

const mapDispatchToProps = (dispatch: Dispatch<ReturnType<typeof getList>>) => {
    return {
        getUserList: () => dispatch(getList())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);
