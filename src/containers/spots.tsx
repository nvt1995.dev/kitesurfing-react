import React, { Dispatch, Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";

import { getList as getFavoritesList } from "@redux/favorites/actions";
import {
    selectError as selectFavoritesError,
    selectList as selectFavoritesList,
    selectLoading as selectFavoritesLoading
} from "@redux/favorites/selectors";
import { getList as getSpotsList, saveItem as saveSpotItem } from "@redux/spots/actions";
import { Spot } from "@redux/spots/models";
import {
    selectError as selectSpotsError,
    selectList as selectSpotsList,
    selectLoading as selectSpotsLoading
} from "@redux/spots/selectors";
import { InitialState } from "@redux/store-models";

import FavoritesMapList from "@components/favoritesMap";
import SpotContent from "@components/spotContent";
import SpotsList from "@components/spotsList";

const SpotsPage = (props: ComponentStoreProps & ComponentDispatchProps) => {
    const { spotsError, spotsLoading, spots, getSpots, saveSpot } = props;
    const { favoritesError, favoritesLoading, favorites, getFavorites } = props;
    const [selectedSpot, setSelectedSpot] = useState<Spot | null>(null);

    useEffect(() => {
        getSpots();
        getFavorites();
    }, [getSpots, getFavorites]);

    const getFavoriteSpots = () => {
        const spotIds = favorites.map((x) => x.spotId);
        return spots.filter((x) => spotIds.includes(x.id));
    };

    return (
        <Fragment>
            {selectedSpot !== null && (
                <SpotContent
                    saveSpot={saveSpot}
                    setSelectedSpot={setSelectedSpot}
                    spot={selectedSpot}
                    loading={favoritesLoading}
                    error={favoritesError}
                />
            )}
            <FavoritesMapList
                maxHeight="50vh"
                favoriteSpots={getFavoriteSpots()}
                loading={favoritesLoading}
                error={favoritesError}
                selectMarker={setSelectedSpot}
            />
            <SpotsList style={{ maxHeight: "35vh" }} spots={spots} loading={spotsLoading} error={spotsError} />
        </Fragment>
    );
};

type ComponentStoreProps = ReturnType<typeof mapStateToProps>;
type ComponentDispatchProps = ReturnType<typeof mapDispatchToProps>;

const mapStateToProps = (state: InitialState) => ({
    spotsLoading: selectSpotsLoading(state.spots),
    spotsError: selectSpotsError(state.spots),
    spots: selectSpotsList(state.spots),
    favoritesLoading: selectFavoritesLoading(state.favorites),
    favoritesError: selectFavoritesError(state.favorites),
    favorites: selectFavoritesList(state.favorites)
});

const mapDispatchToProps = (dispatch: Dispatch<ReturnType<typeof getSpotsList & typeof getFavoritesList>>) => {
    return {
        getSpots: () => dispatch(getSpotsList()),
        getFavorites: () => dispatch(getFavoritesList()),
        saveSpot: (spot: Spot) => dispatch(saveSpotItem(spot))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SpotsPage);
