import React, { Dispatch, useMemo } from "react";
import { connect } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";

import { createMuiTheme, Grid, ThemeProvider, useMediaQuery } from "@material-ui/core";

import { logout as logoutAction } from "@redux/auth/actions";
import { selectIsLoggedIn } from "@redux/auth/selectors";
import { InitialState } from "@redux/store-models";

import NotFoundPage from "@containers/404";
import HomePage from "@containers/home";
import LoginPage from "@containers/login";
import SpotsPage from "@containers/spots";
import UsersPage from "@containers/users";

import Header from "@components/header";

import { MenuItem } from "@models/general";

import GuestRoute from "@guards/guest";
import PrivateRoute from "@guards/private";

const AppPage = (props: ComponentStoreProps & ComponentDispatchProps) => {
    const { isAuthenticated, logout } = props;

    const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");

    const theme = useMemo(
        () =>
            createMuiTheme({
                palette: {
                    type: prefersDarkMode ? "dark" : "light"
                }
            }),
        [prefersDarkMode]
    );

    const menuItems: MenuItem[] = [
        {
            label: "Home",
            path: "/",
            icon: "home",
            component: HomePage,
            exact: true,
            showInMenu: true
        },
        {
            label: "Users",
            path: "/users",
            icon: "people",
            component: UsersPage,
            showInMenu: true
        },
        {
            label: "Spots",
            path: "/spots",
            icon: "room",
            component: SpotsPage,
            showInMenu: true
        },
        {
            label: "Spot",
            path: "/spots/:spotName",
            component: SpotsPage
        }
    ];

    return (
        <ThemeProvider theme={theme}>
            <Grid container direction="column" spacing={2}>
                <Header logout={logout} menuItems={menuItems} isAuthenticated={isAuthenticated} />
                <Switch>
                    <GuestRoute isAuthenticated={isAuthenticated} exact path="/login" component={LoginPage} />
                    {menuItems.map((menuItem, index) => (
                        <PrivateRoute
                            key={index}
                            isAuthenticated={isAuthenticated}
                            exact={menuItem.exact}
                            path={menuItem.path}
                            component={menuItem.component}
                        />
                    ))}
                    <Route path="/404" component={NotFoundPage} />
                    <Redirect from="*" to="/404" />
                </Switch>
            </Grid>
        </ThemeProvider>
    );
};

type ComponentStoreProps = ReturnType<typeof mapStateToProps>;
type ComponentDispatchProps = ReturnType<typeof mapDispatchToProps>;

const mapStateToProps = (state: InitialState) => ({
    isAuthenticated: selectIsLoggedIn(state.auth)
});

const mapDispatchToProps = (dispatch: Dispatch<ReturnType<typeof logoutAction>>) => {
    return {
        logout: () => dispatch(logoutAction())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AppPage);
