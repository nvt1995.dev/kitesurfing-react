import React, { Dispatch } from "react";
import { connect } from "react-redux";

import { Grid } from "@material-ui/core";

import { forceLogin as forceLoginAction } from "@redux/auth/actions";
import { Login } from "@redux/auth/models";
import { selectError, selectLoading } from "@redux/auth/selectors";
import { InitialState } from "@redux/store-models";

import LoginForm from "@components/loginForm";

const LoginPage = (props: ComponentStoreProps & ComponentDispatchProps) => {
    const { login, loading, error } = props;

    const authenticate = (loginData: Login) => {
        login(loginData);
    };

    return (
        <Grid container justify="center" direction="row">
            <Grid item>
                <LoginForm loading={loading} error={error} authenticate={authenticate} />
            </Grid>
        </Grid>
    );
};

type ComponentStoreProps = ReturnType<typeof mapStateToProps>;
type ComponentDispatchProps = ReturnType<typeof mapDispatchToProps>;

const mapStateToProps = (state: InitialState) => ({
    loading: selectLoading(state.auth),
    error: selectError(state.auth)
});

const mapDispatchToProps = (dispatch: Dispatch<ReturnType<typeof forceLoginAction>>) => {
    return {
        login: (data: Login) => dispatch(forceLoginAction())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
