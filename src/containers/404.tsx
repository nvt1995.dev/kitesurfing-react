import React, { useEffect, useState } from "react";
import { Redirect } from "react-router-dom";

import { Grid, Theme, Typography } from "@material-ui/core";
import { createStyles, makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            position: "fixed",
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            color: theme.palette.primary.main
        }
    })
);

const NotFoundPage = () => {
    const classes = useStyles();
    const [secondsToRedirect, setSecondsToRedirect] = useState(10);

    useEffect(() => {
        const timer = setInterval(() => {
            setSecondsToRedirect((prev) => prev - 1);
        }, 1000);

        return () => {
            clearTimeout(timer);
        };
    }, []);

    return secondsToRedirect <= 0 ? (
        <Redirect to="/" />
    ) : (
        <Grid className={classes.container} container direction="column" justify="center" alignItems="center">
            <Grid item>
                <Typography color="inherit" variant="h1">
                    404
                </Typography>
            </Grid>
            <Grid item>
                <Typography color="textPrimary" variant="body2">
                    Redirect in {secondsToRedirect}
                </Typography>
            </Grid>
        </Grid>
    );
};

export default NotFoundPage;
