import * as Models from "./models";

import { Action } from "../store-models";

const initialState: Models.InitialState = {
    loading: false,
    list: []
};

const spotReducers = (state = initialState, action: Action<Models.InitialState> & Action<Models.Spot>) => {
    switch (action.type) {
        case Models.TYPES.GET_LIST: {
            return {
                ...state,
                error: undefined,
                loading: true
            };
        }
        case Models.TYPES.GET_LIST_SUCCESS: {
            return {
                ...state,
                error: undefined,
                loading: false,
                list: action.payload.list
            };
        }
        case Models.TYPES.SAVE_SUCCESS: {
            return {
                ...state,
                error: undefined,
                loading: false,
                list: state.list.map((spot) => (spot.id === action.payload.id ? action.payload : spot))
            };
        }
        case Models.TYPES.DELETE_SUCCESS: {
            return {
                ...state,
                error: undefined,
                loading: false,
                list: state.list.filter((x) => x.id !== action.payload.id)
            };
        }
        case Models.TYPES.GET_LIST_FAIL:
        case Models.TYPES.SAVE_FAIL:
        case Models.TYPES.DELETE_FAIL: {
            return {
                ...state,
                error: action.payload?.error,
                loading: false
            };
        }
        case Models.TYPES.RESET: {
            return initialState;
        }
        default:
            return state;
    }
};

export default spotReducers;
