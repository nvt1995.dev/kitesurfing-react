export enum TYPES {
    GET_LIST = "GET_SPOTS_LIST",
    GET_LIST_SUCCESS = "GET_SPOTS_LIST_SUCCESS",
    GET_LIST_FAIL = "GET_SPOTS_LIST_FAIL",
    SAVE = "SAVE_SPOT",
    SAVE_SUCCESS = "SAVE_SPOT_SUCCESS",
    SAVE_FAIL = "SAVE_SPOT_FAIL",
    DELETE = "DELETE_SPOT",
    DELETE_SUCCESS = "DELETE_SPOT_SUCCESS",
    DELETE_FAIL = "DELETE_SPOT_FAIL",
    RESET = "RESET"
}

export type InitialState = {
    loading: boolean;
    error?: string;
    list: Spot[];
};

export type Spot = {
    month: string;
    probability: number;
    long: string;
    lat: string;
    country: string;
    name: string;
    createdAt: string;
    id: string;
};
