import { Service } from "@redux/service";

import { Spot } from "./models";

export const getList = () => Service.get<Spot[]>("spot", { withCredentials: true });
export const updateSpot = (id: string, spot: Spot) => Service.put<Spot, Spot>(`spot/${id}`, spot, { withCredentials: true });
export const saveSpot = (spot: Spot) => Service.post<Spot, Spot>("spot", spot, { withCredentials: true });
export const deleteSpot = (id: string) => Service.delete<string>(`spot/${id}`, { withCredentials: true });
