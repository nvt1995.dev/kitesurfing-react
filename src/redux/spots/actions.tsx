import { Spot, TYPES } from "./models";

export const getList = () => ({
    type: TYPES.GET_LIST
});

export const getListSuccess = (list: Spot[]) => ({
    type: TYPES.GET_LIST_SUCCESS,
    payload: { list }
});

export const getListFail = (error: string) => ({
    type: TYPES.GET_LIST_FAIL,
    payload: { error }
});

export const saveItem = (spot: Spot) => ({
    type: TYPES.SAVE,
    payload: spot
});

export const updateItemSuccess = (spot: Spot) => ({
    type: TYPES.SAVE_SUCCESS,
    payload: spot
});

export const saveItemFail = (error: string) => ({
    type: TYPES.SAVE_FAIL,
    payload: { error }
});

export const deleteItem = (id: string) => ({
    type: TYPES.DELETE,
    payload: { id }
});

export const deleteItemSuccess = (id: string) => ({
    type: TYPES.DELETE_SUCCESS,
    payload: { id }
});

export const deleteItemFail = (error: string) => ({
    type: TYPES.DELETE_FAIL,
    payload: { error }
});

export const reset = () => ({
    type: TYPES.RESET
});
