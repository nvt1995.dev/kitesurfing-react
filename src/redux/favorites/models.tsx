export enum TYPES {
    GET_LIST = "GET_FAVORITES_LIST",
    GET_LIST_SUCCESS = "GET_FAVORITES_LIST_SUCCESS",
    GET_LIST_FAIL = "GET_FAVORITES_LIST_FAIL",
    SAVE = "SAVE_FAVORITE",
    SAVE_SUCCESS = "SAVE_FAVORITE_SUCCESS",
    SAVE_FAIL = "SAVE_FAVORITE_FAIL",
    DELETE = "DELETE_FAVORITE",
    DELETE_SUCCESS = "DELETE_FAVORITE_SUCCESS",
    DELETE_FAIL = "DELETE_FAVORITE_FAIL",
    RESET = "RESET"
}

export type InitialState = {
    loading: boolean;
    error?: string;
    list: Favorite[];
};

export type Favorite = {
    spotId: string;
    userId: string;
    createdAt: string;
    id: string;
};
