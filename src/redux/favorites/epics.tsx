import { ActionsObservable, combineEpics, ofType } from "redux-observable";

import { of } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";

import * as Actions from "./actions";
import * as Models from "./models";
import * as Services from "./services";

import { Action } from "../store-models";

const getList = (action$: ActionsObservable<Action<undefined>>) =>
    action$.pipe(
        ofType(Models.TYPES.GET_LIST),
        switchMap(() =>
            Services.getList().pipe(
                map((response) => Actions.getListSuccess(response)),
                catchError((err) => of(Actions.getListFail(err.message)))
            )
        )
    );

const saveFavorite = (action$: ActionsObservable<Action<Models.Favorite>>) =>
    action$.pipe(
        ofType(Models.TYPES.SAVE),
        switchMap((action) => {
            if (action.payload.id) {
                return Services.updateFavorite(action.payload.id, action.payload).pipe(
                    map((response) => Actions.updateItemSuccess(response)),
                    catchError((err) => of(Actions.saveItemFail(err.message)))
                );
            } else {
                return Services.saveFavorite(action.payload).pipe(
                    map(() => Actions.getList()),
                    catchError((err) => of(Actions.saveItemFail(err.message)))
                );
            }
        })
    );

const deleteFavorite = (action$: ActionsObservable<Action<string>>) =>
    action$.pipe(
        ofType(Models.TYPES.DELETE),
        switchMap((action) =>
            Services.deleteFavorite(action.payload).pipe(
                map((response) => Actions.deleteItemSuccess(response)),
                catchError((err) => of(Actions.deleteItemFail(err.message)))
            )
        )
    );

export default combineEpics(getList, saveFavorite, deleteFavorite);
