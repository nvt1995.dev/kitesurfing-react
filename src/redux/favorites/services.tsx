import { Service } from "@redux/service";

import { Favorite } from "./models";

export const getList = () => Service.get<Favorite[]>("favourites", { withCredentials: true });
export const updateFavorite = (id: string, favorite: Favorite) =>
    Service.put<Favorite, Favorite>(`favourites/${id}`, favorite, { withCredentials: true });
export const saveFavorite = (favorite: Favorite) => Service.post<Favorite, Favorite>("favourites", favorite, { withCredentials: true });
export const deleteFavorite = (id: string) => Service.delete<string>(`favourites/${id}`, { withCredentials: true });
