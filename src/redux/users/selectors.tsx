import { InitialState } from "./models";

export const selectList = (state: InitialState) => state.list;
export const selectError = (state: InitialState) => state.error;
export const selectLoading = (state: InitialState) => state.loading;
