import { TYPES, User } from "./models";

export const getList = () => ({
    type: TYPES.GET_LIST
});

export const getListSuccess = (list: User[]) => ({
    type: TYPES.GET_LIST_SUCCESS,
    payload: { list }
});

export const getListFail = (error: string) => ({
    type: TYPES.GET_LIST_FAIL,
    payload: { error }
});

export const saveItem = (user: User) => ({
    type: TYPES.SAVE,
    payload: user
});

export const updateItemSuccess = (user: User) => ({
    type: TYPES.SAVE_SUCCESS,
    payload: user
});

export const saveItemFail = (error: string) => ({
    type: TYPES.SAVE_FAIL,
    payload: { error }
});

export const deleteItem = (id: string) => ({
    type: TYPES.DELETE,
    payload: { id }
});

export const deleteItemSuccess = (id: string) => ({
    type: TYPES.DELETE_SUCCESS,
    payload: { id }
});

export const deleteItemFail = (error: string) => ({
    type: TYPES.DELETE_FAIL,
    payload: { error }
});

export const reset = () => ({
    type: TYPES.RESET
});
