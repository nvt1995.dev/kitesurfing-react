import { ActionsObservable, combineEpics, ofType } from "redux-observable";

import { of } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";

import * as Actions from "./actions";
import * as Models from "./models";
import * as Services from "./services";

import { Action } from "../store-models";

const getList = (action$: ActionsObservable<Action<undefined>>) =>
    action$.pipe(
        ofType(Models.TYPES.GET_LIST),
        switchMap(() =>
            Services.getList().pipe(
                map((response) => Actions.getListSuccess(response)),
                catchError((err) => of(Actions.getListFail(err.message)))
            )
        )
    );

const saveUser = (action$: ActionsObservable<Action<Models.User>>) =>
    action$.pipe(
        ofType(Models.TYPES.SAVE),
        switchMap((action) => {
            if (action.payload.id) {
                return Services.updateUser(action.payload.id, action.payload).pipe(
                    map((response) => Actions.updateItemSuccess(response)),
                    catchError((err) => of(Actions.saveItemFail(err.message)))
                );
            } else {
                return Services.saveUser(action.payload).pipe(
                    map(() => Actions.getList()),
                    catchError((err) => of(Actions.saveItemFail(err.message)))
                );
            }
        })
    );

const deleteUser = (action$: ActionsObservable<Action<string>>) =>
    action$.pipe(
        ofType(Models.TYPES.DELETE),
        switchMap((action) =>
            Services.deleteUser(action.payload).pipe(
                map((response) => Actions.deleteItemSuccess(response)),
                catchError((err) => of(Actions.deleteItemFail(err.message)))
            )
        )
    );

export default combineEpics(getList, saveUser, deleteUser);
