export enum TYPES {
    GET_LIST = "GET_USERS_LIST",
    GET_LIST_SUCCESS = "GET_USERS_LIST_SUCCESS",
    GET_LIST_FAIL = "GET_USERS_LIST_FAIL",
    SAVE = "SAVE_USER",
    SAVE_SUCCESS = "SAVE_USER_SUCCESS",
    SAVE_FAIL = "SAVE_USER_FAIL",
    DELETE = "DELETE_USER",
    DELETE_SUCCESS = "DELETE_USER_SUCCESS",
    DELETE_FAIL = "DELETE_USER_FAIL",
    RESET = "RESET"
}

export type InitialState = {
    loading: boolean;
    error?: string;
    list: User[];
};

export type User = {
    id: string;
    name: string;
    avatar: string;
    email: string;
    createdAt: string;
};
