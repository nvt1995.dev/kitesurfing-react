import { Service } from "@redux/service";

import { User } from "./models";

export const getList = () => Service.get<User[]>("user", { withCredentials: true });
export const updateUser = (id: string, user: User) => Service.put<User, User>(`user/${id}`, user, { withCredentials: true });
export const saveUser = (user: User) => Service.post<User, User>("user", user, { withCredentials: true });
export const deleteUser = (id: string) => Service.delete<string>(`user/${id}`, { withCredentials: true });
