import { ActionsObservable, combineEpics, ofType } from "redux-observable";

import { of } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";

import jwt_decode from "jwt-decode";

import * as Actions from "./actions";
import * as Models from "./models";
import * as Services from "./services";

import { Action } from "../store-models";

const TOKEN_KEY = process.env.REACT_APP_TOKEN_KEY || "";

const login = (action$: ActionsObservable<Action<Models.Login>>) =>
    action$.pipe(
        ofType(Models.TYPES.LOGIN),
        switchMap((action) =>
            Services.login(action.payload).pipe(
                map((response) => {
                    localStorage.setItem(TOKEN_KEY, response);
                    let authUser: Models.AuthUser;
                    try {
                        authUser = jwt_decode(response) as Models.AuthUser;
                    } catch (err) {
                        throw err;
                    }

                    return Actions.loginSuccess(authUser);
                }),
                catchError((err) => of(Actions.loginFail(err.message)))
            )
        )
    );

const logout = (action$: ActionsObservable<Action<undefined>>) =>
    action$.pipe(
        ofType(Models.TYPES.LOGOUT),
        map(() => {
            localStorage.removeItem(TOKEN_KEY);
            return Actions.resetAll();
        })
    );

export default combineEpics(login, logout);
