import { InitialState } from "./models";

export const selectIsLoggedIn = (state: InitialState) => state.isLoggedIn;
export const selectAuthUser = (state: InitialState) => state.authUser;
export const selectError = (state: InitialState) => state.error;
export const selectLoading = (state: InitialState) => state.loading;
