import { AuthUser, Login, TYPES } from "./models";

export const login = (loginData: Login) => ({
    type: TYPES.LOGIN,
    payload: loginData
});

export const logout = () => ({
    type: TYPES.LOGOUT
});

export const loginSuccess = (authUser: AuthUser) => ({
    type: TYPES.LOGIN_SUCCESS,
    payload: { authUser }
});

export const loginFail = (error: string) => ({
    type: TYPES.LOGIN_FAIL,
    payload: { error }
});

export const forceLogin = () => ({
    type: TYPES.FORCE_LOGIN
});

export const resetAll = () => ({
    type: TYPES.RESET_ALL
});
