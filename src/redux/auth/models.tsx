import { User } from "@redux/users/models";

export enum TYPES {
    LOGIN = "LOGIN",
    LOGOUT = "LOGOUT",
    LOGIN_SUCCESS = "LOGIN_SUCCESS",
    LOGIN_FAIL = "LOGIN_FAIL",
    FORCE_LOGIN = "FORCE_LOGIN",
    RESET_ALL = "RESET"
}

export type InitialState = {
    isLoggedIn: boolean;
    authUser?: AuthUser;
    loading: boolean;
    error?: string;
};

export type Login = {
    email: string;
    password: string;
};

export type AuthUser = {} & User;
