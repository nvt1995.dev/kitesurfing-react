import { Service } from "@redux/service";

import { Login } from "./models";

export const login = (data: Login) => Service.post<string, Login>("login", data);
