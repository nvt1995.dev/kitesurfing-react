import * as Models from "./models";

import { Action } from "../store-models";

const initialState: Models.InitialState = {
    loading: false,
    isLoggedIn: false
};

const authReducers = (state = initialState, action: Action<Models.InitialState>) => {
    switch (action.type) {
        case Models.TYPES.LOGIN: {
            return {
                ...state,
                error: undefined,
                loading: true,
                authUser: undefined,
                isLoggedIn: false
            };
        }
        case Models.TYPES.LOGIN_SUCCESS: {
            return {
                ...state,
                error: undefined,
                loading: false,
                authUser: action.payload?.authUser,
                isLoggedIn: true
            };
        }
        case Models.TYPES.LOGIN_FAIL: {
            return {
                ...state,
                error: action.payload?.error,
                loading: false,
                authUser: undefined,
                isLoggedIn: false
            };
        }
        case Models.TYPES.FORCE_LOGIN: {
            return {
                ...state,
                isLoggedIn: true
            }
        }
        case Models.TYPES.LOGOUT: {
            return initialState;
        }
        default:
            return state;
    }
};

export default authReducers;
