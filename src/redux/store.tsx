import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { combineEpics, createEpicMiddleware } from "redux-observable";

import AxiosInterceptor from "@redux/service-interceptor";

import authEpics from "./auth/epics";
import authReducers from "./auth/reducers";
import favoriteEpics from "./favorites/epics";
import favoriteReducers from "./favorites/reducers";
import spotEpics from "./spots/epics";
import spotReducers from "./spots/reducers";
import userEpics from "./users/epics";
import userReducers from "./users/reducers";

const composeEnhancer = composeWithDevTools({
    name: "Kitesurfing"
});
const epics = combineEpics(authEpics, userEpics, spotEpics, favoriteEpics);
const epicMiddleware = createEpicMiddleware();
const reducers = combineReducers({ auth: authReducers, users: userReducers, spots: spotReducers, favorites: favoriteReducers });
const store = createStore(reducers, undefined, composeEnhancer(applyMiddleware(epicMiddleware)));
epicMiddleware.run(epics);

AxiosInterceptor(store.dispatch);

export default store;
