import { InitialState as AuthInitialState } from "./auth/models";
import { InitialState as FavoritesInitialState } from "./favorites/models";
import { InitialState as SpotsInitialState } from "./spots/models";
import { InitialState as UsersInitialState } from "./users/models";

export type Action<T> = {
    type: string;
    payload: T;
};

export type InitialState = {
    auth: AuthInitialState;
    users: UsersInitialState;
    spots: SpotsInitialState;
    favorites: FavoritesInitialState;
};
