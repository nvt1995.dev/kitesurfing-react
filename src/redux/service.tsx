import { from } from "rxjs";

import axios, { AxiosRequestConfig } from "axios";

const API = process.env.REACT_APP_API;

export const Service = {
    get: <R extends {}>(url: string, config?: AxiosRequestConfig) =>
        from(axios.get<R>(API + url, config).then((response) => response.data)),
    post: <R extends {}, T extends {}>(url: string, data: T, config?: AxiosRequestConfig) =>
        from(axios.post<R>(API + url, data, config).then((response) => response.data)),
    put: <R extends {}, T extends {}>(url: string, data: T, config?: AxiosRequestConfig) =>
        from(axios.put<R>(API + url, data, config).then((response) => response.data)),
    patch: <R extends {}, T extends {}>(url: string, data: T, config?: AxiosRequestConfig) =>
        from(axios.patch<R>(API + url, data, config).then((response) => response.data)),
    delete: <R extends {}>(url: string, config?: AxiosRequestConfig) =>
        from(axios.delete<R>(API + url, config).then((response) => response.data))
};
