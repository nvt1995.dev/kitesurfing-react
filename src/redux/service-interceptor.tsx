import { Dispatch } from "react";

import axios from "axios";

import { logout } from "./auth/actions";

const TOKEN_KEY = process.env.TOKEN_KEY || "";

const AxiosInterceptor = (dispatch: Dispatch<ReturnType<typeof logout> & any>) => {
    axios.interceptors.request.use(
        (config) => {
            if (config.withCredentials) {
                const token = localStorage.getItem(TOKEN_KEY);
                // config.headers = {
                //     Authorization: `Bearer ${token}`,
                //     Accept: "application/json"
                // };
                config.withCredentials = false;
            }
            return config;
        },
        (error) => {
            Promise.reject(error);
        }
    );

    axios.interceptors.response.use(
        (response) => {
            return response;
        },
        (error) => {
            if (error.response.status === 403) {
                dispatch(logout());
            }
            return Promise.reject(error);
        }
    );
};

export default AxiosInterceptor;
