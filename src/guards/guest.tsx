import React from "react";
import { Redirect, Route } from "react-router-dom";

import { CustomRouteProps } from "@models/general";

const PrivateRoute = ({ component, isAuthenticated, location, ...rest }: CustomRouteProps) => {
    return !isAuthenticated ? (
        <Route {...rest} component={component} />
    ) : (
        <Redirect to={location ? location.state?.from || { pathname: "/" } : "/"} />
    );
};

export default PrivateRoute;
