import { ComponentType } from "react";
import { RouteProps } from "react-router-dom";

import { Location as HistoryLocation } from "history";

export type CustomRouteProps = RouteProps & {
    isAuthenticated: boolean;
    location?: HistoryLocation<{ from: Location }>;
};

export type MenuItem = {
    path: string;
    icon?: string;
    label: string;
    component: ComponentType;
    exact?: boolean;
    showInMenu?: boolean;
};
